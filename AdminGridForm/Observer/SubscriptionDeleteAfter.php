<?php
declare(strict_types=1);

namespace Ogilvy\Pepe\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Ogilvy\Pepe\Model\Data\Subscription;
use Psr\Log\LoggerInterface;

class SubscriptionDeleteAfter implements ObserverInterface
{
    /**
     * @param LoggerInterface $logger
     */
    public function __construct(
        private LoggerInterface $logger
    ) { }

    /**
     * @inheritDoc
     */
    public function execute(Observer $observer): void
    {
        try {
            /** @var Subscription $subscription */
            $subscription = $observer->getEvent()->getDataObject();

            if (get_class($subscription) === Subscription::class) {

                $this->logger->info(
                    "Deleted 'Subscription' with ID: " . $subscription->getId()
                );
                return;
            }

            $this->logger->warning(
                "Received event: " . $observer->getEvent()->getName() .
                " but did not find any entity inside."
            );

        } catch (\Exception $e) {
            // We don't want to throw exception because this class just logs to file
            // and don't want to interrupt main Entity process
            $this->logger->error($e->getMessage());
        }
    }
}
