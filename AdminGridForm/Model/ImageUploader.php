<?php
declare(strict_types=1);

namespace Ogilvy\Pepe\Model;


use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\UrlInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;

class ImageUploader
{
    public const SAVE_IMG_PATH = 'subs/images/';
    private array $allowedExtensions;

    /**
     * @param Filesystem $filesystem
     * @param UploaderFactory $uploaderFactory
     * @param StoreManagerInterface $storeManager
     * @param LoggerInterface $logger
     */
    public function __construct(
        private readonly Filesystem            $filesystem,
        private readonly UploaderFactory       $uploaderFactory,
        private readonly StoreManagerInterface $storeManager,
        private readonly LoggerInterface       $logger
    ) {
        $this->allowedExtensions = ['jpg', 'jpeg', 'gif', 'png'];
    }

    /**
     * @param $imageUpload
     * @return array
     * @throws LocalizedException
     */
    public function saveFileToImgDir($imageUpload): array
    {
        $result = [];

        try {
            // We save image into folder: MEDIA + TMP_SAVE_IMG_PATH
            $mediaDirectory = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA);
            $target = $mediaDirectory->getAbsolutePath(self::SAVE_IMG_PATH);

            $uploader = $this->uploaderFactory->create(['fileId' => $imageUpload]);
            $uploader->setAllowedExtensions($this->allowedExtensions);
            $uploader->setAllowRenameFiles(true);
            $result = $uploader->save($target);
            if ($result) {
                $result['url'] = $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA)
                    . self::SAVE_IMG_PATH
                    . $result['file'];

                $result['name'] = $result['file'];
                $result['path'] = 'media/' . self::SAVE_IMG_PATH . $result['file'];
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            throw new LocalizedException(__('Something went wrong while saving the file(s).'));
        }

        return $result;
    }
}
