<?php
declare(strict_types=1);

namespace Ogilvy\Pepe\Model\ResourceModel;

use Ogilvy\Pepe\Api\Data\SubscriptionInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Subscription extends AbstractDb
{
    public const TABLE = 'ogilvy_pepe_subscription';

    /**
     * @var string
     */
    protected $_eventPrefix = 'ogilvy_pepe_subscription_resourcemodel';

    protected function _construct()
    {
        $this->_init(self::TABLE, SubscriptionInterface::ID);
    }
}
