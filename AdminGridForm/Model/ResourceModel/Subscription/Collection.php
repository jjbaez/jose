<?php
namespace Ogilvy\Pepe\Model\ResourceModel\Subscription;

use Ogilvy\Pepe\Api\Data\SubscriptionInterface;
use Ogilvy\Pepe\Model\Data\Subscription as Model;
use Ogilvy\Pepe\Model\ResourceModel\Subscription as ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'ogilvy_pepe_subscription_collection';

    protected $_idFieldName = SubscriptionInterface::ID;

    /**
     * Initialize collection model.
     */
    protected function _construct()
    {
        $this->_init(Model::class, ResourceModel::class);
    }
}
