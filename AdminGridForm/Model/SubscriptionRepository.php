<?php
declare(strict_types=1);

namespace Ogilvy\Pepe\Model;

use Ogilvy\Pepe\Api\SubscriptionRepositoryInterface;
use Ogilvy\Pepe\Api\Data\SubscriptionInterface;
use Ogilvy\Pepe\Api\Data\SubscriptionInterfaceFactory;
use Ogilvy\Pepe\Api\Data\SubscriptionResultsInterface;
use Ogilvy\Pepe\Api\Data\SubscriptionResultsInterfaceFactory;
use Ogilvy\Pepe\Model\ResourceModel\Subscription as ResourceModelSubscription;
use Ogilvy\Pepe\Model\ResourceModel\Subscription\CollectionFactory;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class SubscriptionRepository implements SubscriptionRepositoryInterface
{
    /**
     * @param SubscriptionInterfaceFactory $subsFactory
     * @param ResourceModelSubscription $resourceModelSubscription
     * @param CollectionFactory $subsCollectionFactory
     * @param SubscriptionResultsInterfaceFactory $subscriptionResultsInterfaceFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        private SubscriptionInterfaceFactory        $subsFactory,
        private ResourceModelSubscription           $resourceModelSubscription,
        private CollectionFactory                   $subsCollectionFactory,
        private SubscriptionResultsInterfaceFactory $subscriptionResultsInterfaceFactory,
        private CollectionProcessorInterface        $collectionProcessor
    ) { }

    /**
     * @inheritdoc
     */
    public function get(int $id): ?SubscriptionInterface
    {
        $entity = $this->subsFactory->create();
        $this->resourceModelSubscription->load($entity, $id);

        if (!$entity->getId()) {
            return null;
        }

        return $entity;
    }

    /**
     * @inheritDoc
     */
    public function save(SubscriptionInterface $subscription): SubscriptionInterface
    {
        $this->resourceModelSubscription->save($subscription);

        return $subscription;
    }

    /**
     * @inheritDoc
     */
    public function delete(SubscriptionInterface $subscription): bool
    {
        $entityId = $subscription->getId();

        try {
            $this->resourceModelSubscription->delete($subscription);
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(
                __(
                    'Unable to remove Subscription with subscription_id = %1 because of Error = %2',
                    $entityId,
                    $e->getMessage()
                )
            );
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById(int $id): bool
    {
        return $this->delete($this->get($id));
    }

    /**
     * @inheritDoc
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SubscriptionResultsInterface
    {
        $collection = $this->subsCollectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);

        $searchResults = $this->subscriptionResultsInterfaceFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function getBySubscriberEmail(string $email): ?SubscriptionInterface
    {
        $entity = $this->subsFactory->create();
        $this->resourceModelSubscription->load($entity, $email, "subscriber_email");

        if (!$entity->getId()) {
            return null;
        }
        return $entity;
    }
}
