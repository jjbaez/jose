<?php
declare(strict_types=1);

namespace Ogilvy\Pepe\Model\Subscription;

use Ogilvy\Pepe\Api\Data\SubscriptionInterface;
use Ogilvy\Pepe\Model\ResourceModel\Subscription\CollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;

class DataProvider extends AbstractDataProvider
{

    private array $loadedData;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param StoreManagerInterface $storeManager
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        string                        $name,
        string                        $primaryFieldName,
        string                        $requestFieldName,
        CollectionFactory             $collectionFactory,
        private StoreManagerInterface $storeManager,
        array                         $meta = [],
        array                         $data = []
    )
    {
        $this->collection = $collectionFactory->create();
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );
    }

    public function getData(): array
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        $this->loadedData = [];

        $baseurl = $this->storeManager->getStore()->getBaseUrl();

        /** @var SubscriptionInterface $model */
        foreach ($items as $model) {
            $temp = $model->getData();

            // We need to recreate IMAGE array for the ImageUplodaer widget.
            if ($temp['image_path']) {
                $img = [];
                $img[0]['name'] = basename($temp['image_path']);
                $img[0]['file'] = basename($temp['image_path']);
                $img[0]['full_path'] = basename($temp['image_path']);
                $img[0]['path'] = $temp['image_path'];
                $img[0]['url'] = $baseurl . $temp['image_path'];
                $img[0]['type'] = "image/jpeg";
                $temp['image'] = $img;
                $this->loadedData[$model->getId()]['export_fieldset'] = array_merge($model->getData(), $temp);
                continue;
            }
            $this->loadedData[$model->getId()]['export_fieldset'] = $model->getData();
        }

        return $this->loadedData;
    }
}
