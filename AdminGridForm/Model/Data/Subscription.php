<?php
declare(strict_types=1);

namespace Ogilvy\Pepe\Model\Data;

use Ogilvy\Pepe\Api\Data\SubscriptionInterface;
use Ogilvy\Pepe\Model\ResourceModel\Subscription as ResourceModel;

use Magento\Framework\Model\AbstractModel;

class Subscription extends AbstractModel implements SubscriptionInterface
{

    // $_eventPrefix - a prefix for events to be triggered
    protected $_eventPrefix = 'ogilvy_pepe_subscribtion_model';

    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }

    /**
     * @inheritDoc
     */
    public function getSubscriberEmail(): ?string
    {
        return $this->getData(self::SUBSCRIBER_EMAIL);
    }

    /**
     * @inheritDoc
     */
    public function setSubscriberEmail(?string $email): void
    {
        $this->setData(self::SUBSCRIBER_EMAIL, $email);
    }

    /**
     * @inheritDoc
     */
    public function getMediaName(): ?string
    {
        return $this->getData(self::MEDIA_NAME);
    }

    /**
     * @inheritDoc
     */
    public function setMediaName(?string $mediaName): void
    {
        $this->setData(self::MEDIA_NAME, $mediaName);
    }

    /**
     * @inheritDoc
     */
    public function getProductSku(): string
    {
        return $this->getData(self::PRODUCT_SKU);
    }

    /**
     * @inheritDoc
     */
    public function setProductSku(string $productSku): void
    {
        $this->setData(self::PRODUCT_SKU, $productSku);
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt(): string
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * @inheritDoc
     */
    public function setCreatedAt(string $createdAt): void
    {
        $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * @inheritDoc
     */
    public function getSubscriptionEnd(): ?string
    {
        return $this->getData(self::END_SUBSCRIPTION);
    }

    /**
     * @inheritDoc
     */
    public function setSubscriptionEnd(?string $subscriptionEnd): void
    {
        $this->setData(self::END_SUBSCRIPTION, $subscriptionEnd);
    }

    /**
     * @inheritDoc
     */
    public function getIsActive(): bool
    {
        return (bool) $this->getData(self::END_SUBSCRIPTION);
    }

    /**
     * @inheritDoc
     */
    public function setIsActive(bool $isActive): void
    {
        $this->setData(self::IS_ACTIVE, $isActive);
    }

    /**
     * @inheritDoc
     */
    public function getImagePath(): ?string
    {
        return $this->getData(self::IMAGE_PATH);
    }

    /**
     * @inheritDoc
     */
    public function setImagePath(?string $imagePath): void
    {
        $this->setData(self::IMAGE_PATH, $imagePath);
    }
}
