<?php

declare(strict_types=1);

namespace Ogilvy\Pepe\Model\Data;

use Ogilvy\Pepe\Api\Data\SubscriptionResultsInterface;
use Magento\Framework\Api\SearchResults;

class SubscriptionSearchResults extends SearchResults implements SubscriptionResultsInterface
{
}
