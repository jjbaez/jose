<?php
declare(strict_types=1);

namespace Ogilvy\Pepe\Controller\Adminhtml\Subscription;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Filesystem\Driver\File;
use Ogilvy\Pepe\Api\SubscriptionRepositoryInterface;

class Delete extends Action
{
    /**
     * @param SubscriptionRepositoryInterface $subsRepository
     * @param File $file
     * @param Context $context
     */
    public function __construct(
        private readonly SubscriptionRepositoryInterface $subsRepository,
        private readonly File $file,
        Context $context
    ) {
        parent::__construct($context);

    }

    /**
     * @return Redirect
     */
    public function execute(): Redirect
    {
        $redirect = $this->resultRedirectFactory->create();
        try {
            $subId = (int) $this->getRequest()->getParam('subscription_id');

            $this->deleteImageFromServer($subId);

            $this->subsRepository->deleteById($subId);

            $this->messageManager->addSuccessMessage(__("Subscription deleted succesfully."));

        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }
        return $redirect->setPath('*/*/index');
    }

    /**
     * We can use this method here, or embed this method in Subscriptionepository::delete()
     * so we can forget about deleting Image files everytime we want to delete a Subscription.
     *
     * @param $subId
     * @return void
     */
    private function deleteImageFromServer($subId): void
    {
        try {
            $subscription = $this->subsRepository->get($subId);
            $imagePath = $subscription->getImagePath();
            if ($this->file->isExists($imagePath)) {
                // We must check if file exists, becuase if it doesnt,
                // "deleteFile" throws exception.
                $this->file->deleteFile($imagePath);
            }
        } catch (\Exception $e) {
            // We don't care about any error while deleting file.
        }
    }
}
