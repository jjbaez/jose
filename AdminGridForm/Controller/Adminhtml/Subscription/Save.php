<?php
declare(strict_types=1);

namespace Ogilvy\Pepe\Controller\Adminhtml\Subscription;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Phrase;
use Ogilvy\Pepe\Api\Data\SubscriptionInterface;
use Ogilvy\Pepe\Api\Data\SubscriptionInterfaceFactory;
use Ogilvy\Pepe\Api\SubscriptionRepositoryInterface;

class Save extends Action implements HttpPostActionInterface
{
    /**
     * @param Context $context
     * @param SubscriptionRepositoryInterface $subsRepository
     * @param SubscriptionInterfaceFactory $subsFactory
     */
    public function __construct(
        Context                                 $context,
        private SubscriptionRepositoryInterface $subsRepository,
        private SubscriptionInterfaceFactory    $subsFactory
    )
    {
        parent::__construct($context);
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        try {
            $postBody = $this->getRequest()->getParam('export_fieldset');
            if (!$this->isHttpBodyValid($postBody)) {
                throw new LocalizedException(new Phrase(__('HTTP Post body is not complete.')));
            }

            /** @var SubscriptionInterface $subscription */
            $subscription = $this->subsFactory->create();

            $subscription->setData($postBody);
            // Extract image path from array and add to Entity
            $imagePath = $postBody["image"][0]["path"] ?? null;
            $subscription->setImagePath($imagePath);

            $this->subsRepository->save($subscription);

            $this->messageManager->addSuccessMessage(
                __('Subscription added succesfully for user "%1".', $postBody['subscriber_email'])
            );

        } catch (\Throwable $e) {
            $error = __('Subscription could not be added: %1', $e->getMessage());
            $this->messageManager->addErrorMessage($error);
        }
        return $resultRedirect->setPath('*/*/index');
    }

    /**
     * Checks if any of the HTTP POST parameters is empty
     *
     * @param array $body
     * @return bool
     */
    private function isHttpBodyValid(array $body): bool
    {
        return !(empty($body)
            || empty($body['subscriber_email'])
            || empty($body['media_name'])
            || empty($body['product_sku'])
            || empty($body['subscription_end']));
    }

    /**
     * @return bool
     */
    protected function _isAllowed() : bool
    {
        return $this->_authorization->isAllowed(self::ADMIN_RESOURCE);
    }
}
