<?php
declare(strict_types=1);

namespace Ogilvy\Pepe\Controller\Adminhtml\Subscription;

use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\Page;

class Create extends Action implements HttpGetActionInterface
{
    public function execute(): Page
    {
        $pageResult = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $title = $pageResult->getConfig()->getTitle();
        $title->prepend(__('Add New Subscription'));

        return $pageResult;
    }

    /**
     * @inheritDoc
     */
    protected function _isAllowed(): bool
    {
        return $this->_authorization->isAllowed(Index::ADMIN_RESOURCE);
    }
}
