<?php
declare(strict_types=1);

namespace Ogilvy\Pepe\Api;

use Ogilvy\Pepe\Api\Data\SubscriptionInterface;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Ogilvy\Pepe\Api\Data\SubscriptionResultsInterface;

/**
 * @api
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
interface SubscriptionRepositoryInterface
{
    /**
     * @param int $id
     * @return SubscriptionInterface|null
     * @throws NoSuchEntityException
     */
    public function get(int $id): ?SubscriptionInterface;

    /**
     * @param SubscriptionInterface $subscription
     * @return SubscriptionInterface
     * @throws AlreadyExistsException
     */
    public function save(SubscriptionInterface $subscription): SubscriptionInterface;

    /**
     * @param SubscriptionInterface $subscription
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(SubscriptionInterface $subscription): bool;

    /**
     * @param int $id
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function deleteById(int $id): bool;

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return SubscriptionResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Column "subscriber_email" in DDBD should have a BTREE index created
     * to speedup searching when table grows.
     *
     * @param string $email
     * @return SubscriptionInterface|null
     * @throws NoSuchEntityException
     */
    public function getBySubscriberEmail(string $email): ?SubscriptionInterface;
}
