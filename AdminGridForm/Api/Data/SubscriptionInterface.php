<?php
declare(strict_types=1);

namespace Ogilvy\Pepe\Api\Data;

interface SubscriptionInterface
{
    public const ID = 'subscription_id';
    public const SUBSCRIBER_EMAIL = 'subscriber_email';
    public const MEDIA_NAME = 'media_name';
    public const PRODUCT_SKU = 'product_sku';
    public const CREATED_AT = 'created_at';
    public const END_SUBSCRIPTION = 'subscription_end';
    public const IS_ACTIVE = 'is_active';
    public const IMAGE_PATH = 'image_path';

    /**
     * @return string|null
     */
    public function getSubscriberEmail(): ?string;

    /**
     * @param string|null $email
     *
     * @return void
     */
    public function setSubscriberEmail(?string $email): void;

    /**
     * @return string|null
     */
    public function getMediaName(): ?string;

    /**
     * @param string|null $mediaName
     *
     * @return void
     */
    public function setMediaName(?string $mediaName): void;

    /**
     * @return string
     */
    public function getProductSku(): string;

    /**
     * @param string $productSku
     * @return void
     */
    public function setProductSku(string $productSku): void;

    /**
     * @return string
     */
    public function getCreatedAt(): string;

    /**
     * @param string $createdAt
     * @return void
     */
    public function setCreatedAt(string $createdAt): void;

    /**
     * @return string|null
     */
    public function getSubscriptionEnd(): ?string;

    /**
     * @param string|null $subscriptionEnd
     * @return void
     */
    public function setSubscriptionEnd(?string $subscriptionEnd): void;

    /**
     * @return bool
     */
    public function getIsActive(): bool;

    /**
     * @param bool $isActive
     * @return void
     */
    public function setIsActive(bool $isActive): void;

    /**
     * @return string|null
     */
    public function getImagePath(): ?string;

    /**
     * @param string|null $imagePath
     * @return void
     */
    public function setImagePath(?string $imagePath): void;

}
