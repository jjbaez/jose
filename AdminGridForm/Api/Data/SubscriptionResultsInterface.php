<?php
declare(strict_types=1);

namespace Ogilvy\Pepe\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * @api
 */
interface SubscriptionResultsInterface extends SearchResultsInterface
{
    /**
     * @return \Ogilvy\Pepe\Api\Data\SubscriptionInterface[]
     */
    public function getItems();

    /**
     * @param \Ogilvy\Pepe\Api\Data\SubscriptionInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
