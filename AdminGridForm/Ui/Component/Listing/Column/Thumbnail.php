<?php
declare(strict_types=1);

namespace Ogilvy\Pepe\Ui\Component\Listing\Column;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

class Thumbnail extends Column
{

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param StoreManagerInterface $storeManager
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface                       $context,
        UiComponentFactory                     $uiComponentFactory,
        private readonly StoreManagerInterface $storeManager,
        private readonly UrlInterface          $urlBuilder,
        array                                  $components = [],
        array                                  $data = [],
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @param array $dataSource
     * @return array
     * @throws NoSuchEntityException
     */
    public function prepareDataSource(array $dataSource): array
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                $url = isset($item[$fieldName]) ?
                    $this->storeManager->getStore()->getBaseUrl() . $item[$fieldName] : '';
                $item[$fieldName . '_src'] = $url;
                $item[$fieldName . '_alt'] = basename($url);
                $item[$fieldName . '_orig_src'] = $url;
                $item[$fieldName . '_link'] = $this->urlBuilder
                    ->getUrl(
                        'ogilvy_pepe/subscription/create',
                        ['subscription_id' => $item['subscription_id']]
                    );
            }
        }
        return $dataSource;
    }
}
