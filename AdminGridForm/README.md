# Ogilvy_Pepe module

This module creates a grid table to manage "subscriptions" for a media (newspaper, magacine, etc)
The fields used are:
```
subscription_id     > main table ID
subscriber_email    > email of the customer
media_name          > name of the media company
product_sku         > SKU of the subscription
created_at          > date of creation
subscription_end    > date of end of subscription
image_path          > loca lfile path of some image
```

**SKUS and emails are not linked** to real entities from Magento. They are random values.

## Installation details

* Unzip files to your magento installation: `app/code/Ogilvy/Pepe/`


* Run commands:

`bin/magento module:enable Ogilvy_Pepe`

`bin/magento se:up`

`bin/magento c:f`


* You can insert some example rows to newly created table: `ogilvy_pepe_subscription` with following SQL:
```
TRUNCATE ogilvy_pepe_subscription;
INSERT INTO ogilvy_pepe_subscription (subscriber_email, media_name, product_sku, created_at, subscription_end, is_active)VALUES
("aaa@test.com", "name 01", "aaa-mxf-222", "2010-02-02", "2012-12-01", false),
("bbb@test.com", "name 012", "bbbmxf-333", "2011-04-12", "2012-12-01", false),
("ccc@test.com", "name 0145", "abc-mxf-002", "2012-06-22", "2013-12-01", false),
("ddd@test.com", "name 3431", "abc-mxf-002", "2023-08-10", "2024-12-01", true),
("eee@test.com", "name 4341", "dsafds-mxf-111", "2024-01-11", "2024-12-01", true),
("fff@test.com", "name 04451", "abc-mxf-002", "2024-03-02", "2024-12-01", true);
```
 
## WebApi endpoint

* To use REST endpoint you must use **Admin** user, or a normal user with specific ACL permission : `Ogilvy_Pepe::subscription_list`


* First, make login through REST using a POST request to: `http://URL_MAGENTO/rest/V1/integration/admin/token`
with JSON payload:
```
{
  "username": "admin",
  "password": "YOUR_PASSWORD"
}
```

* URL to get the full list of entities: `http://URL_MAGENTO/rest/V1/subscriptions/list?searchCriteria[]`


* URL to search for entities "created_at" > 2024-01-01 :

```
http://URL_MAGENTO/rest/V1/subscriptions/list?searchCriteria
   [filter_groups][0][filters][0][field]=created_at
   &searchCriteria[filter_groups][0][filters][0][value]=2024-01-01 00:00:00
   &searchCriteria[filter_groups][0][filters][0][condition_type]=gt`
```



__
