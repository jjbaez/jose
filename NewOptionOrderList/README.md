# README #

## Adds new option to EXPORT TO FILE, in SALES >> ORDERS ##

![New option](Docs/screenNewOption.jpg)

It exports all items from orders list, instead of exporting Orders information.

If there is same product ID in different orders, export will update 'quantity'
for these repeated Product ID's.

