<?php
declare (strict_types = 1);

namespace Jose\NewOptionOrderList\Model\Export;

use Magento\Catalog\Api\ProductRepositoryInterface;

use Magento\Ui\Component\MassAction\Filter;

use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Api\OrderItemRepositoryInterface;

use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 *  Generates a csv file with product information from the selected orders
 *
 * Product information in each line:
 * - product sku
 * - product name
 * - quantity ordered
 */
class ConvertToCsv
{
    private const COLUMN_SKU = 'sku';
    private const COLUMN_NAME = 'name';
    private const COLUMN_QUANTITY = 'quantity';

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var Filter
     */
    private $filter;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var OrderItemRepositoryInterface
     */
    private $orderItemRepository;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @param Filesystem $filesystem
     * @param Filter $filter
     * @param ProductRepositoryInterface $productRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder $filterBuilder
     * @param OrderItemRepositoryInterface $orderItemRepository
     */
    public function __construct(
        Filesystem $filesystem,
        Filter $filter,
        ProductRepositoryInterface $productRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        OrderItemRepositoryInterface $orderItemRepository
    ) {
        $this->filesystem = $filesystem;
        $this->filter = $filter;
        $this->productRepository = $productRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->orderItemRepository = $orderItemRepository;
    }

    /**
     * Returns CSV file
     *
     * @return array
     * @throws LocalizedException
     */
    public function getCsvFile()
    {
        $component = $this->filter->getComponent();

        $name = md5(microtime());
        $file = 'export/' . $component->getName() . $name . '.csv';

        $this->filter->prepareComponent($component);
        $this->filter->applySelectionOnTargetProvider();

        $directory = $this->filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $directory->create('export');
        $stream = $directory->openFile($file, 'w+');
        $stream->lock();
        $stream->writeCsv([self::COLUMN_SKU, self::COLUMN_NAME, self::COLUMN_QUANTITY]);

        $dataProvider = $component->getContext()->getDataProvider();

        $gridItems = $dataProvider->getSearchResult();

        $products = $this->getUnifiedProducts($gridItems->getAllIds());
        
        $totalCount = (int) $gridItems->getTotalCount();
        if ($totalCount > 0) {
            foreach ($products as $item) {
                $stream->writeCsv(
                    [
                        $item[self::COLUMN_SKU],
                        $item[self::COLUMN_NAME],
                        (int) $item[self::COLUMN_QUANTITY]
                    ]
                );
            }
        }
        $stream->unlock();
        $stream->close();

        return [
            'type' => 'filename',
            'value' => $file,
            'rm' => true
        ];
    }

    /**
     * Gets all products grouped by ProductID from specific Orders.
     * 'Quantity' will be updated for every duplicated ProductID.
     *
     * @param $orderIds
     * @return array
     */
    private function getUnifiedProducts($orderIds): array
    {
        $itemsGrouped = [];
        foreach ($this->getProductsFromOrdersIds($orderIds) as $item) {
            if (!array_key_exists($item->getProductId(), $itemsGrouped)) {
                try {

                    $product = $this->productRepository->get($item->getSku());

                } catch (NoSuchEntityException $e) {
                    $this->logger->debug(sprintf('Missing product with sku %s', $item->getSku()));
                }

                $itemsGrouped[$item->getProductId()] = [
                    self::COLUMN_SKU => $item->getSku(),
                    self::COLUMN_NAME => $item->getName(),
                    self::COLUMN_QUANTITY => 0
                ];
            }
            $itemsGrouped[$item->getProductId()][self::COLUMN_QUANTITY] += $item->getQtyOrdered();
        }
        return $itemsGrouped;
    }

    /**
     * Gets all Products from specific Orders.
     * Products can be duplicated because same ProductID's
     * can be in different Orders.
     *
     * @param $orderIds
     * @return OrderItemInterface[]
     */
    private function getProductsFromOrdersIds($orderIds): array
    {
        $filter[] = $this->filterBuilder
            ->setField('order_id')
            ->setConditionType('in')
            ->setValue($orderIds)
            ->create();

        $searchCriteria = $this->searchCriteriaBuilder->addFilters($filter);

        return $this->orderItemRepository->getList($searchCriteria->create())->getItems();
    }
}
