<?php
namespace Jose\MassActionPdf\Plugin;

use Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Sales\Controller\Adminhtml\Invoice\Pdfinvoices as MagePdfInvoices;
use Magento\Sales\Model\Order\Pdf\Invoice;
use Jose\MassActionPdf\Model\Db;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;
use ZipArchive;

class PdfInvoices
{
    private const FOLDER_1 = 'josetest';
    private const FOLDER_2 = 'invoices';
    private const FOLDER_3 = 'pdf';

    /**
     * @var DirectoryList
     */
    protected $filesystem;

    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * @var DateTime
     */
    protected $dateTime;

    /**
     * @var Invoice
     */
    protected $pdfInvoice;
    /**
     * @var File
     */
    private $ioFile;

    /**
     * @param Filesystem $filesystem
     * @param Invoice $pdfInvoice
     * @param DateTime $dateTime
     * @param FileFactory $fileFactory
     * @param File $ioFile
     */
    public function __construct(
        Filesystem $filesystem,
        Invoice $pdfInvoice,
        DateTime $dateTime,
        FileFactory $fileFactory,
        File $ioFile
    ) {
        $this->filesystem = $filesystem;
        $this->ioFile = $ioFile;
        $this->pdfInvoice = $pdfInvoice;
        $this->dateTime = $dateTime;
        $this->fileFactory = $fileFactory;
    }

    /**
     * 1.) Se crearán varias subcarpetas en var: josetest/invoices/pdf
     * 2.) Ahí dentro se guardarán todos los PDF de forma independiente.
     * 3.) Se zipearan en josetest/invoices/fichero.zip
     * 4.) Se envía al usuario el ZIP.
     * 
     * NOTA: El ZIP se crea bien y es descomprimible, pero la descarga web
     *       manda un ZIP corrompido.
     * 
     * @param MagePdfInvoices $subject
     * @param callable $proceed
     * @param AbstractCollection $collection
     * @return ResponseInterface
     * @throws Exception
     */
    public function aroundMassAction(MagePdfInvoices $subject, callable $proceed, AbstractCollection $collection)
    {

        $varFolder = $this->filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $pdfPath = implode(DIRECTORY_SEPARATOR, [self::FOLDER_1, self::FOLDER_2, self::FOLDER_3]);
        $pdfAbsolutePath = $varFolder->getAbsolutePath($pdfPath);

        if (!is_dir($pdfAbsolutePath)) {
            $this->ioFile->mkdir($pdfAbsolutePath);
        }

        foreach ($collection as $item) {

            $pdfContent = $this->pdfInvoice->getPdf([$item]);

            $pdfName = sprintf('invoice%s%s.pdf', $this->dateTime->date('Y-m-d_H-i-s'), rand());

            $this->ioFile->write($pdfAbsolutePath . '/' . $pdfName, $pdfContent->render());
        }

        $zipPath = implode(DIRECTORY_SEPARATOR, [self::FOLDER_1, self::FOLDER_2]);
        $zipAbsolutePath = $varFolder->getAbsolutePath($zipPath);
        $zipFileName = sprintf('invoices_%s.zip', $this->dateTime->date('Y-m-d'));
        $zipAbsolutePathFile = $zipAbsolutePath . '/' . $zipFileName;

        $zip = new ZipArchive();
        $zip->open($zipAbsolutePathFile, ZipArchive::CREATE);

        $filesToDelete = [];

        /**
         * @var SplFileInfo
         */
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($zipAbsolutePath), RecursiveIteratorIterator::LEAVES_ONLY);

        foreach ($files as $name => $file) {
            // Skip directories (they would be added automatically)
            if (!$file->isDir()) {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($zipAbsolutePath) + 1);

                // Add current file to archive
                $zip->addFile($filePath, $relativePath);

                $filesToDelete[] = $filePath;
            }
        }
        $zip->close();

        foreach ($filesToDelete as $file) {
            unlink($file);
        }


        $fileContent = ['type' => 'filename', 'value' => $varFolder->getRelativePath($zipPath . '/' . $zipFileName), 'rm' => true];
        return $this->fileFactory->create(
            $zipFileName,
            $fileContent,
            DirectoryList::VAR_DIR,
            'application/zip'
        );
    }

}
