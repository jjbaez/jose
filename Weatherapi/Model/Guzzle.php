<?php
declare(strict_types=1);

namespace Jose\Weatherapi\Model;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\ClientFactory as GuzzleClientFactory;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ResponseFactory;

use Magento\Framework\Webapi\Rest\Request;

use Psr\Log\LoggerInterface;

class Guzzle
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Data
     */
    private $systemConfig;

    /**
     * @var GuzzleClientFactory
     */
    private $guzzleClientFactory;

    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * @param LoggerInterface $logger
     * @param Data $systemConfig
     * @param GuzzleClientFactory $guzzleClientFactory
     * @param ResponseFactory $responseFactory
     */
    public function __construct(
        LoggerInterface $logger,
        Config $systemConfig,
        GuzzleClientFactory $guzzleClientFactory,
        ResponseFactory $responseFactory
    ) {
        $this->logger = $logger;
        $this->systemConfig = $systemConfig;
        $this->guzzleClientFactory = $guzzleClientFactory;
        $this->responseFactory = $responseFactory;
    }

    /**
     * Send HTTP request to URL
     *
     * @param string $url
     * @param string $url_endpoint
     * @param string $req_method
     * @return Response
     */
    public function connectToUrl(string $url, string $url_endpoint, string $req_method = null): Response
    {
        // Sanitize input arguments for Guzzle Http:
        //    1.) Force '/' at the end of base URL
        //    2.) Remove '/' at the beginning of Endpoint
        $url = rtrim($url, '/') . '/';
        $url_endpoint = ltrim($url_endpoint, '/');

        /** @var GuzzleClient $client */
        $client = $this->guzzleClientFactory->create(['config' => [
            'base_uri' => $url
        ]]);

        /** @var Response $response */
        $response = null;
        if ($req_method === null) {
            $req_method = Request::HTTP_METHOD_GET;
        }
        try {
            $response = $client->request(
                $req_method,
                $url_endpoint
            );

        } catch (GuzzleException $e) {
            $response = $this->responseFactory->create([
                'status' => 400,
                'reason' => $e->getMessage()
            ]);
        }

        $this->logResponse($response);

        return $response;
    }

    /**
     * @param Response $httpResponse
     */
    private function logResponse(Response $httpResponse): void
    {
        if ($httpResponse->getStatusCode() === 200) {
            $ugly_json = \GuzzleHttp\json_decode($httpResponse->getBody(), true);

            $pretty_json = \GuzzleHttp\json_encode($ugly_json, JSON_PRETTY_PRINT);

            $this->logger->info('Jose\Weatherapi. Guzzle: Status 200. Response Body is: ' . $pretty_json);

        } else {
            $this->logger->error('Jose\Weatherapi. Guzzle exception: body is: ' . $httpResponse->getReasonPhrase());
        }
    }
}