<?php
declare (strict_types=1);

namespace Jose\Weatherapi\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;

use Magento\Store\Model\ScopeInterface;

class Config
{
    private const APIKEY = 'section_guzzletest01/group_guzzletest01/guzzle_apikey';
    private const GEOIPKEY = 'section_guzzletest01/group_guzzletest01/geoip_key';

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return string|null
     */
    public function getWeatherApiKey()
    {
        return $this->scopeConfig->getValue(self::APIKEY, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return string|null
     */
    public function getGeoIpKey()
    {
        return $this->scopeConfig->getValue(self::GEOIPKEY, ScopeInterface::SCOPE_STORE);
    }

}