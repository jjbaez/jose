<?php
declare (strict_types=1);

namespace Jose\Weatherapi\Model;

use Jose\Weatherapi\Api\GeoIpServiceInterface;

use Magento\Framework\Exception\NotFoundException;

use GuzzleHttp\Psr7\Response;

use Psr\Log\LoggerInterface;

class GeoIpService implements GeoIpServiceInterface
{

    private const URL = 'http://api.ipstack.com/';

    /**
     * @var Guzzle
     */
    private $guzzle;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Data
     */
    private $systemConfig;

    /**
     * @param LoggerInterface $logger
     * @param Guzzle $guzzle
     * @param Data $systemConfig
     */
    public function __construct(
        LoggerInterface $logger,
        Guzzle $guzzle,
        Config $systemConfig
    ) {
        $this->guzzle = $guzzle;
        $this->logger = $logger;
        $this->systemConfig = $systemConfig;
    }

    /**
     * @param string $ipAddress
     * @return string
     */
    public function getLocationByIpAddress(string $ipAddress): string
    {

        $httpGeoIpQuery = http_build_query(
            [
                '?access_key' => $this->systemConfig->getGeoIpKey()
            ]
        );

        $urlEndpoint = $ipAddress . urldecode($httpGeoIpQuery);

        $httpResponse = $this->guzzle->connectToUrl(self::URL, $urlEndpoint);

        return ($this->processGeoHttpResponse($httpResponse));
    }

    /**
     * @param Response $httpResponse
     * @return string
     */
    private function processGeoHttpResponse(Response $httpResponse): string
    {
        if ($httpResponse->getStatusCode() === 200) {

            $json = \GuzzleHttp\json_decode($httpResponse->getBody(), true);
            $city = $json['city'];

        } else {

            $this->logger->error('Jose\Weatherapi: Guzzle exception: ' . $httpResponse->getReasonPhrase());
        }

        if (!$city || isNull($city)) {

            throw new NotFoundException(
                __('Couldn\'t retrieve City by IpAddress. Probably because Magento and the user\'s computer are on same private LAN.')
            );
        }

        return ($city);
    }
}
