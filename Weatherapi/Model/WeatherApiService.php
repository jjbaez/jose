<?php
declare (strict_types=1);

namespace Jose\Weatherapi\Model;

use Jose\Weatherapi\Api\GeoIpServiceInterface;
use Jose\Weatherapi\Api\WeatherApiServiceInterface;

use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Webapi\Rest\Request as ApiRequest;

use GuzzleHttp\Psr7\Response;

use Psr\Log\LoggerInterface;

class WeatherApiService implements WeatherApiServiceInterface
{
    private const BASE_URL = 'http://api.openweathermap.org/';
    private const ENDPOINT = 'data/2.5/forecast';

    /**
     * @var Guzzle
     */
    private $guzzle;

    /**
     * @var Data
     */
    private $systemConfig;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var ApiRequest
     */
    protected $apiRequest;

    /**
     * @var GeoIpServiceInterface
     */
    protected $geoIp;

    private $url;
    private $url_endpoint;
    private $api_key;

    /**
     * @param LoggerInterface $logger
     * @param Guzzle $guzzle
     * @param Config $systemConfig
     * @param ApiRequest $apiRequest
     * @param GeoIpServiceInterface $geoIp
     */
    public function __construct(
        LoggerInterface $logger,
        Guzzle $guzzle,
        Config $systemConfig,
        ApiRequest $apiRequest,
        GeoIpServiceInterface $geoIp
    ) {
        $this->guzzle = $guzzle;
        $this->logger = $logger;
        $this->systemConfig = $systemConfig;
        $this->apiRequest = $apiRequest;
        $this->geoIp = $geoIp;
    }

    /**
     * Entry function which:
     *  1) Fills OpenWeather URL parametes.
     *  2) Connects to OpenWeather and process its response.
     *
     * @return array
     */
    public function getWeatherByLocation(): array
    {
        $this->getUrlEndpointParameters();

        $httpResponse = $this->guzzle->connectToUrl(self::BASE_URL, $this->url_endpoint);

        return ($this->processWeatherHttpResponse($httpResponse));
    }

    /**
     * Get OpenWeather URL parameters:
     *    + Client's Ip Address
     *    + Client's City
     *    + OpenWeather API Key
     *
     * @return void
     */
    private function getUrlEndpointParameters()
    {
        $ipAddress = $this->apiRequest->getClientIp();

        try {

            $city = $this->geoIp->getLocationByIpAddress($ipAddress);

        } catch (NotFoundException $e) {

            $errorMessage = $e->getMessage() .
                'City will be overrided with some default value, for example, \'Brighton\'';

            $this->logger->error('Jose\Weatherapi. ' . $errorMessage);
            
            $city='Brighton';
        }

        $httpQuery = urldecode(http_build_query(
            [
                '?q' => $city,
                'appid' =>  $this->systemConfig->getWeatherApiKey(),
                'units' => 'metric'
            ]
        ));

        $this->url_endpoint = self::ENDPOINT . $httpQuery;
    }

    /**
     * @param Response $httpResponse
     * @return array
     */
    private function processWeatherHttpResponse(Response $httpResponse): array
    {

        if ($httpResponse->getStatusCode() === 200) {

            $json = \GuzzleHttp\json_decode($httpResponse->getBody(), true);
            
            foreach ($json['list'] as $entry) {
                
                foreach ($entry['weather'] as $weather) {
                    $arr[]= $weather['main'];
                    $arr[]= $weather['description'];
                }
                $this->response[] = [
                    'epoch' =>  $entry['dt'],
                    'weather' => $arr,
                    'rain' => isset($entry['rain']['3h'])?$entry['rain']['3h']:0,
                    'wind' => $entry['wind']['speed'],
                    'pressure' => $entry['main']['pressure'],
                    'humidity' => $entry['main']['humidity'],
                    'temp' => $entry['main']['temp'],
                    'cloud' => $entry['clouds']['all'],
                ];
            }
            
        } else {

            $this->response[] = ['status' =>
                ['0' => [
                    'code' => $httpResponse->getStatusCode(),
                    'message' => $httpResponse->getReasonPhrase(),
                ],
                ]];

            $this->logger->error('guzzle exception: ' . $httpResponse->getReasonPhrase());
        }

        return ($this->response);
    }
}
