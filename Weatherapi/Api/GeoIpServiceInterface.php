<?php
declare (strict_types=1);

namespace Jose\Weatherapi\Api;

/**
 * It gives name of the city where user is connecting from.
 *
 * @api
 */
interface GeoIpServiceInterface
{
    /**
     * @return string
     */
    public function getLocationByIpAddress(string $ipAddress): string;
}
