<?php
declare (strict_types=1);

namespace Jose\Weatherapi\Api;

/**
 * It gives 5-day forecast from user's location.
 *
 * BASE URL: http://api.openweathermap.org/
 * Endpoint: data/2.5/forecast
 *
 * @api
 */
interface WeatherApiServiceInterface
{
    /**
     * @return array
     */
    public function getWeatherByLocation(): array;
}