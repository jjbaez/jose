# README #
## Context ##

Magento frontend wants to show weather forecast for the next 5 days to the user, taking into account his/her real location.

Frontend will send AJJAX requests to Magento server, and Magento will connect to OpenWeather API to collect info.

## Development ##

Weather Data needed by Frontend:

- Weather  
- Wind  
- Temperature  
- Precipitations  
- Cloud  
- Humidity  
- Pressure  

## Requirements ##

- Weather API: https://openweathermap.org/forecast5  
  *This API is free for 1.000 requests per month. Create a free account to get the KEY and use it on your development.*

- All the information from the API will be requested from backend. The frontend will not access directly to openweathermap API, only Magento.

- The URL to try this demo is: 'http:/{Magento_URL}/rest/V1/api/weather'



