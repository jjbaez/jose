<?php
declare(strict_types=1);

namespace Jose\ImportItems\Cron;

use Jose\ImportItems\Model\Config;
use Jose\ImportItems\Model\ImportFromFtp;
use Magento\Framework\Exception\LocalizedException;
use Psr\Log\LoggerInterface;

class Cron
{
    private $logger;
    private $import;
    private $systemConfig;

    public function __construct(
        LoggerInterface $logger,
        ImportFromFtp $import,
        Config $systemConfig
    ) {
        $this->logger = $logger;
        $this->import = $import;
        $this->systemConfig = $systemConfig;
    }

    /**
     * @throws LocalizedException
     */
    public function execute()
    {
        // Comprobamos si el checkbox en el backend está habilitado.
        // Es mas facil deshabilitar a través de este checkbox que
        // que borrar el cronjob
        if ($this->systemConfig->getFtpCronEnabled()) {

            $this->logger->info('Cronjob_Josetest: Se inicia el cron');
            
            $this->import->execute();

        } else {

            $this->logger->info('Cronjob_Josetest: Se ha intentado iniciar el cron pero está deshabilitado en el backend.');
        }
    }
}
