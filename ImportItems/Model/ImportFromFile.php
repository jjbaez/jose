<?php
declare(strict_types=1);

namespace Jose\ImportItems\Model;

use Magento\Framework\Exception\FileSystemException;
use Psr\Log\LoggerInterface;

class ImportFromFile
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(
        LoggerInterface $logger
    ) {
        $this->logger = $logger;
    }

    /**
     * @param $csvFullPath
     * @return string
     * @throws FileSystemException
     */
    public function execute($csvFullPath)
    {
        $this->logger->info('Import.php: ImportFromFile. Nos llamaron del Cronjob.');

        if (file_exists($csvFullPath)) {
            $csvInput = file_get_contents($csvFullPath);
            // Funcion que convierte de CSV al XML de BigBridge
            // $this->import->convertCsvToXml($csvInput);
        } else {
            return (sprintf('\n File not found in : %s \n ', $csvFullPath));
        }
    }
}
