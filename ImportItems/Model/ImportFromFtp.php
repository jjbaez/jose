<?php
declare(strict_types=1);

namespace Jose\ImportItems\Model;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem\Io\Ftp;
use Psr\Log\LoggerInterface;

class ImportFromFtp
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Ftp
     */
    private $ftp;

    private $systemConfig;

    /**
     * @param LoggerInterface $logger
     * @param Ftp $ftp
     * @param Config $systemConfig
     */
    public function __construct(
        LoggerInterface $logger,
        Ftp $ftp,
        Config $systemConfig
    ) {
        $this->logger = $logger;
        $this->ftp = $ftp;
        $this->systemConfig = $systemConfig;
    }

    /**
     * @throws LocalizedException
     */
    public function execute()
    {
        $this->logger->info('Import.php: ImportFromFtp. Nos llamaron del Cronjob.');

        $content = $this->getFileContent();

        // Onetic
        // $this->import->convertCsvToXml($content);
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    private function getFileContent(): string
    {
        return $this->getConnector()->read($this->systemConfig->getFtpFileName());
    }

    /**
     * @return Ftp
     * @throws LocalizedException
     */
    private function getConnector()
    {
        if (!$this->systemConfig->getFtpAddress()
            || !$this->systemConfig->getFtpUsername()
            || !$this->systemConfig->getFtpPassword()
            || !$this->systemConfig->getFtpFileName()
        ) {
            throw new LocalizedException(__('FTP configuration is missing'));
        }

        $open = $this->ftp->open(
            [
                    'host' => $this->systemConfig->getFtpAddress(),
                    'user' => $this->systemConfig->getFtpUsername(),
                    'password' => $this->systemConfig->getFtpPassword(),
                    'passive' => true
            ]
        );
        return $this->ftp;
    }
}
