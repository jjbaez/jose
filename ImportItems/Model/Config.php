<?php
declare(strict_types=1);

namespace Jose\ImportItems\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Config
{
    private const FTP_ADDRESS = 'sectionImport/general/ftp_address';
    private const FTP_USERNAME = 'sectionImport/general/ftp_username';
    private const FTP_PASSWORD = 'sectionImport/general/ftp_password';
    private const FTP_CRON_ENABLED = 'sectionImport/general/ftp_cron_enabled';
    private const FTP_FILENAME = 'sectionImport/general/ftp_filename';
   
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return string
     */
    public function getFtpAddress(): string
    {
        return (string) $this->scopeConfig->getValue(self::FTP_ADDRESS, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return string
     */
    public function getFtpUsername(): string
    {
        return $this->scopeConfig->getValue(self::FTP_USERNAME, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return string
     */
    public function getFtpPassword():string
    {
        return  $this->scopeConfig->getValue(self::FTP_PASSWORD, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return bool
     */
    public function getFtpCronEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag(self::FTP_CRON_ENABLED, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return string
     */
    public function getFtpFileName(): string
    {
        return $this->scopeConfig->getValue(self::FTP_FILENAME, ScopeInterface::SCOPE_STORE);
    }

}
