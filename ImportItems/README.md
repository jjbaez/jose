## IMPORT PRODUCTS FROM .CSV STORED IN REMOTE FTP SERVER ##

**Objective:** download a CSV file from an FTP server and import products to Magento.

FTP configuration in:  STORES >> CONFIGURATION >> IMPORT FILE >> FTP CONFIG

Two ways to run the download from FTP:

  1.) With command line:
            `bin/magento Jose:import:download`


  2.) With Cronjob.
    Cronjob group "josetest" has been created.
    You should enable cronjobs in Magento (cron:install) and in my "FTP CONFIG" as well.
    Linux Cronjobs are run every 3 hours by default in "crontab.xml"


I added a DataPatch to create categories and attributes needed for the new products before we import products.


Once CSV downloaded, it will import products using BigBridge "product-import".
For this we have to convert from CSV to BigBridge's XML previously.


As the CSV I tried was downloaded from a real store I deleted all code (caterogries, attributes, etc) from my code.
