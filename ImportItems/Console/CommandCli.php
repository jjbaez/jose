<?php
declare(strict_types=1);

namespace Jose\ImportItems\Console;

use Jose\ImportItems\Model\ImportFromFile;
use Jose\ImportItems\Model\ImportFromFtp;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CommandCli extends Command
{
    private const OPTION_ONLYCONVERT = 'fromFile';

    private $import;

    /**
     * @var ImportFromFtp
     */
    private $importFromFtp;

    /**
     * @var ImportFromFile
     */
    private $importFromFile;

    /**
     * @param ImportFromFtp $importFromFtp
     * @param ImportFromFile $importFromFile
     */
    public function __construct(
        ImportFromFtp $importFromFtp,
        ImportFromFile $importFromFile
    ) {
        $this->importFromFtp = $importFromFtp;
        $this->importFromFile = $importFromFile;

        parent::__construct();
    }

    protected function configure()
    {
        $commandoptions = [new InputOption(
            self::OPTION_ONLYCONVERT,
            null,
            InputOption::VALUE_OPTIONAL,//mirar
            'Only convert CSV to XML, not download from FTP'
        )];

        $this->setName('Jose:import:download');
        $this->setDescription('Download CSV file from FTP');
        $this->setDefinition($commandoptions);

        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($fullPath = $input->getOption(self::OPTION_ONLYCONVERT)) {
            $output->writeln('<info>Converting from CSV file already downloaded(not from FTP). Option disabled.');
            //$this->importFromFile->execute($fullPath);
        } else {
            $output->writeln('<info>CLI Command executing....</info>');
            $this->importFromFtp->execute();
        }
        $output->writeln('<info>CLI Command finished.</info>');
    }
}
