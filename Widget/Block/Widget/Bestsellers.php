<?php
declare (strict_types = 1);

namespace Jose\Widget\Block\Widget;

use Magento\Catalog\Helper\Image;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Helper\Cart;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Pricing\Render;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Model\ResourceModel\Report\Bestsellers\Collection;
use Magento\Sales\Model\ResourceModel\Report\Bestsellers\CollectionFactory;
use Magento\Widget\Block\BlockInterface;

class Bestsellers extends Template implements BlockInterface
{
    protected $_template = 'widget/bestseller.phtml';

    const DEFAULT_PRODUCTS_COUNT = 5;
    const DEFAULT_PERIOD = 'year';
    const DEFAULT_IMAGE_WIDTH = 150;
    const DEFAULT_IMAGE_HEIGHT = 150;

    /**
     * @var CollectionFactory
     */
    protected $bestSellerFactory;

    /**
     * @var Image
     */
    protected $imageHelper;

    /**
     * @var Cart
     */
    protected $cartHelper;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @param Context $context
     * @param Image $imageHelper
     * @param Cart $cartHelper
     * @param ProductRepositoryInterface $productRepository
     * @param CollectionFactory $bestSellerFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Image $imageHelper,
        Cart $cartHelper,
        ProductRepositoryInterface $productRepository,
        CollectionFactory $bestSellerFactory,
        array $data = []
    ) {
        $this->imageHelper = $imageHelper;
        $this->cartHelper = $cartHelper;
        $this->productRepository = $productRepository;
        $this->bestSellerFactory = $bestSellerFactory;

        parent::__construct($context, $data);
    }

    /**
     * @return Image
     */
    public function imageHelperObj()
    {
        return $this->imageHelper;
    }

    /**
     * @return Collection
     */
    public function getBestsellerProduct()
    {
        $bestSellerCollection = $this->bestSellerFactory->create()
            ->setModel(\Magento\Catalog\Model\Product::class)
            ->setPeriod($this->getPeriod());

        $bestSellerCollection->setPageSize($this->getProductLimit());

        return $bestSellerCollection;
    }

    /**
     * @param $id
     * @return Product
     */
    public function getLoadProduct($id)
    {
       return $this->productRepository->getById($id);
    }

    /**
     * @return int
     */
    public function getProductLimit(): int
    {
        if ($this->getData('product_count') === '') {
            return self::DEFAULT_PRODUCTS_COUNT;
        }
        return (int) $this->getData('product_count');
    }

    /**
     * @return string
     */
    public function getPeriod(): string
    {
        if ($this->getData('period') === '') {
            return self::DEFAULT_PERIOD;
        }
        return $this->getData('period');
    }

    /**
     * @return int
     */
    public function getProductImageWidth(): int
    {
        if ($this->getData('image_width') === '') {
            return self::DEFAULT_IMAGE_WIDTH;
        }
        return (int) $this->getData('image_width');
    }

    /**
     * @return int
     */
    public function getProductImageHeight(): int
    {
        if ($this->getData('image_height') === '') {
            return self::DEFAULT_IMAGE_HEIGHT;
        }
        return (int) $this->getData('image_height');
    }

    /**
     * @param $product
     * @param array $additional
     * @return string
     */
    public function getAddToCartUrl($product, $additional = [])
    {
        return $this->cartHelper->getAddUrl($product, $additional);
    }

    /**
     *  Return HTML block with price
     *
     * @param Product $product
     * @param null $priceType
     * @param string $renderZone
     * @param array $arguments
     * @return string
     * @throws LocalizedException
     */
    public function getProductPriceHtml(
        Product $product,
        $priceType = null,
        $renderZone = Render::ZONE_ITEM_LIST,
        array $arguments = []
    ) {
        if (!isset($arguments['zone'])) {
            $arguments['zone'] = $renderZone;
        }
        $arguments['zone'] = $arguments['zone'] ?? $renderZone;
        $arguments['price_id'] = $arguments['price_id'] ?? 'old-price-' . $product->getId() . '-' . $priceType;
        $arguments['include_container'] = $arguments['include_container'] ?? true;
        $arguments['display_minimal_price'] = $arguments['display_minimal_price'] ?? true;

        /** @var Render $priceRender */
        $priceRender = $this->getLayout()->getBlock('product.price.render.default');

        $price = '';
        if ($priceRender) {
            $price = $priceRender->render(
                \Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE,
                $product,
                $arguments
            );
        }
        return $price;
    }
}
