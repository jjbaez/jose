## BestSeller Widget ##

Widget to put on main page or any other categories page within Magento, top, bottom, etc..

![New Option](Docs/screen.jpg)


1.) Check that you already have INVOICES in your Magento.

2.) Go to STATISTICS >> REFRESH STATISTICS (it will update tables: "sales_bestsellers_aggregated_***" ) because BestSellers information is stored in those tables.

3.) Go to CONTENT >> WIDGET >> ADD WIDGET >> Choose "BestSeller Product Widget"

4.) "LAYOUT UPDATES" >> choose what pages to show Widget on. E.g. "CMS Home Page" >> "Content.Top"

5.) Check "WIDGET OPTIONS" 

6.) "SAVE" widget


