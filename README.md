# README #

Magento 2 learning exercises:

 * DynamicRow
 * Guzzle
 * ImportItems (download a CSV from FTP server and import its products)
 * AroundPlugin
 * BestSellers Widget
 * NewExportOption 
 * AddNewColumnToGrid
 * Offline Shipping Method
 * Offline Payment Method
 * Connection to REST Api (WeatherApi)
 * Add new Address  attributes to Checkout, Adminhtml and Customer (AddCheckout)
 * Creates a custom entity + triad DB. Admin grid. Admin Form to create/edit entities. ImageUploader. WebApi Endpoint

### Magento Associate Developer Certification (2020): ###
https://www.youracclaim.com/badges/3df19df0-4067-4e3f-851c-f79d48569e65

### Adobe Commerce Expert Certification (2023): ###
https://www.credly.com/badges/85172464-84df-462a-98f3-81b1054d9637/public_url
