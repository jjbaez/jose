<?php
declare (strict_types=1);

namespace Jose\AddColumnToGrid\Plugin;

use Magento\Sales\Model\ResourceModel\Order\Grid\Collection as OrderGridCollection;

use Magento\Framework\View\Element\UiComponent\DataProvider\CollectionFactory;

use Psr\Log\LoggerInterface;

/**
 * AfterGetReport Plugin which adds product items
 * to "product name" column in sales_order_grid
 */
class AddDataToOrdersGrid
{
    /**
     * @var LoggerInterface
     */
    private $logger;
 
    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger   = $logger;
    }
 
    /**
     * @param CollectionFactory $subject
     * @param OrderGridCollection $collection
     * @param $requestName
     *
     * @return mixed
     */
    public function afterGetReport($subject, $collection, $requestName)
    {
        if ($requestName !== 'sales_order_grid_data_source') {
            return $collection;
        }
 
        if ($collection->getMainTable() === $collection->getResource()->getTable('sales_order_grid')) {
            try {
                // Get original table name
                $orderItemsTableName = $collection->getResource()->getTable('sales_order_item');

                // Create new select instance
                $itemsTableSelectGrouped = $collection->getConnection()->select();

                // Add table with columns which must be selected (skip useless columns)
                $itemsTableSelectGrouped->from(
                    $orderItemsTableName,
                    [
                        'name'     => new \Zend_Db_Expr('GROUP_CONCAT(DISTINCT name SEPARATOR \',\')'),
                        'order_id' => 'order_id'
                    ]
                );

                // Group our select to make one column for one order
                $itemsTableSelectGrouped->group('order_id');

                // Add our sub-select to main collection with only one column: name
                $collection->getSelect()
                            ->joinLeft(
                                ['soi' => $itemsTableSelectGrouped],
                                'soi.order_id = main_table.entity_id',
                                ['name']
                            );

            } catch (\Zend_Db_Select_Exception $selectException) {
                $this->logger->error($selectException);
            }
        }
 
        return $collection;
    }
}
