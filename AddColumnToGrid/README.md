# README #

### Add New Column to Grid and make it Searchable ###

1.) Added new colum "Product Items" to Grid. It will show the items purchased in each Order.

At first, it will be blank because we have to populate the column by ourselves.

![Added Column](Docs/screen_01.gif)




2.) Our data is stored in a different table than "sales_order_grid" so we must JOIN both tables to add data to our column.

We can use a Plugin "afterGetReport" which will populate the column and, also, it will  make the column searchable at the same time.


_


![Column populated](Docs/screen_02.gif)


*(Column filled with data)*


_



![Searchable Column](Docs/screen_03.gif)


*(We can filter rows by "product items")*


