<?php
declare(strict_types=1);

namespace Jose\Guzzle\Model;

use Magento\Config\Model\Config\Backend\Admin\Custom;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Data
{
    private const URL_ADDRESS = 'section_guzzletest01/group_guzzletest01/guzzle_urladdress';
    private const URL_ENDPOINT = 'section_guzzletest01/group_guzzletest01/guzzle_urlendpoint';
    private const APIKEY = 'guzzle_apikey';


    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    public function getUrlAddress(): string
    {
        return (string) $this->scopeConfig->getValue(self::URL_ADDRESS, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return string
     */
    public function getUrlEndpoint(): string
    {
        return (string) $this->scopeConfig->getValue(self::URL_ENDPOINT, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return (string) $this->scopeConfig->getValue(self::APIKEY, ScopeInterface::SCOPE_STORE);
    }

}
