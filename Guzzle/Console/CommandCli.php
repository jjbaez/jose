<?php
declare (strict_types = 1);

namespace Jose\Guzzle\Console;

use Jose\Guzzle\Model\Guzzle;
use Magento\Framework\Exception\LocalizedException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CommandCli extends Command
{
    private const OPTION_URLENDPOINT = 'url';

    /**
     * @var Guzzle
     */
    private $guzzle;

    /**
     * @param Guzzle $guzzle
     */
    public function __construct(Guzzle $guzzle)
    {
        $this->guzzle = $guzzle;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('Jose:guzzle:test');
        $this->setDescription('Enviar peticion con Guzzle Http Client');

        $commandoptions = [new InputOption(
            self::OPTION_URLENDPOINT,
            null,
            InputOption::VALUE_OPTIONAL,
            'Por si necesitamos una opcion...'
        )];

        $this->setDefinition($commandoptions);

        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>Conectando a la URL...</info>');

        $this->guzzle->execute();

        $output->writeln('<info>CLI Command finished.</info>');
    }
}
