
##  CONNECT TO ENDPOINT WITH GUZZLE CLIENT ##

1.) Go to STORES >> CONFIGURATION >> GUZZLE TEST

2.) Fill in "URL address" and "URL Endpoint".
    Destination must output a well-formed JSON.
   
3.) Run test from command line:
    bin/magento jose:guzzle:test

4.) Output will be logged in var/log/debug.log


  I made this module to try a different HTTP client which is integrated in Magento.
It looks a bit easier to configure but, at the end, it is using CURL underneath.

