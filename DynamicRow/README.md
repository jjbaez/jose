
Just a quick test to learn how to add "Dynamic rows" when inputting data in adminhtml forms.

![DynamicRowtext](Docs/screenDynamicRow.jpg)

1.) Go to STORES >> CONFIGURATION >> DYNAMIC ROW

2.) Test with numbers and/or letters. Only numbers allowed.
