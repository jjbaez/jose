<?php
declare(strict_types=1);

namespace Jose\DynamicRow\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Ranges
 * @package Jose\DynamicRow\Block\Adminhtml\Form\Field
 */
class Ranges extends AbstractFieldArray
{

    /**
     * Prepare rendering the new field by adding all the needed columns
     */
    protected function _prepareToRender()
    {
        $this->addColumn('width', ['label' => __('Width'), 'class' => 'required-entry validate-number']);
        $this->addColumn('height', ['label' => __('Height'), 'class' => 'required-entry validate-number']);
        $this->addColumn('diameter', ['label' => __('Diameter'), 'class' => 'required-entry validate-number']);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }

    /**
     * Prepare existing row data object
     *
     * @param DataObject $row
     * @throws LocalizedException
     */
    protected function _prepareArrayRow(DataObject $row): void
    {
        $options = [];
        $row->setData('option_extra_attrs', $options);
    }
}
