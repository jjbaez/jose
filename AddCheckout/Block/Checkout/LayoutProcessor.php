<?php
declare (strict_types = 1);

namespace Jose\AddCheckout\Block\Checkout;

use Magento\Checkout\Block\Checkout\LayoutProcessorInterface;

use Magento\Eav\Api\AttributeRepositoryInterface;

class LayoutProcessor implements LayoutProcessorInterface
{

    /**
     * @var AttributeRepositoryInterface
     */
    private $attributeRepository;

    /**
     * @param AttributeRepositoryInterface $attributeRepository
     */
    public function __construct(
        AttributeRepositoryInterface $attributeRepository
    ) {
        $this->attributeRepository = $attributeRepository;
    }

    /**
     * @param array $result
     * @return array
     */
    public function process($result)
    {
        $result = $this->getShippingFormFields($result);
        $result = $this->getBillingFormFields($result);
        return $result;
    }

    /**
     * @param array $result
     * @return array
     */
    public function getShippingFormFields($result): array
    {
        if (isset($result['components']['checkout']['children']['steps']['children']
            ['shipping-step']['children']['shippingAddress']['children']
            ['shipping-address-fieldset'])
        ) {
            $shippingCustomFields = $this->getFields('shippingAddress.custom_attributes');

            // Usual fields (name, fax, country, etc)
            $shippingFields = $result['components']['checkout']['children']['steps']['children']
                ['shipping-step']['children']['shippingAddress']['children']
                ['shipping-address-fieldset']['children'];

            $shippingFields = array_replace_recursive($shippingFields, $shippingCustomFields);

            $result['components']['checkout']['children']['steps']['children']
            ['shipping-step']['children']['shippingAddress']['children']
            ['shipping-address-fieldset']['children'] = $shippingFields;
        }

        return $result;
    }

    /**
     * @param array $result
     * @return array
     */
    public function getBillingFormFields($result): array
    {
        if (isset($result['components']['checkout']['children']['steps']['children']
            ['billing-step']['children']['payment']['children']
            ['payments-list'])) {
            $paymentForms = $result['components']['checkout']['children']['steps']['children']
                ['billing-step']['children']['payment']['children']
                ['payments-list']['children'];

            foreach ($paymentForms as $paymentMethodForm => $paymentMethodValue) {
                $paymentMethodCode = str_replace('-form', '', $paymentMethodForm);

                if (!isset($result['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'][$paymentMethodCode . '-form'])) {
                    continue;
                }

                $billingFields = $result['components']['checkout']['children']['steps']['children']
                    ['billing-step']['children']['payment']['children']
                    ['payments-list']['children'][$paymentMethodCode . '-form']['children']['form-fields']['children'];

                $billingCustomFields = $this->getFields('billingAddress' . $paymentMethodCode . '.custom_attributes');

                $billingFields = array_replace_recursive($billingFields, $billingCustomFields);

                $result['components']['checkout']['children']['steps']['children']
                ['billing-step']['children']['payment']['children']
                ['payments-list']['children'][$paymentMethodCode . '-form']['children']['form-fields']['children'] = $billingFields;
            }
        }

        return $result;
    }

    /**
     * @param string $scope
     * @return array
     */
    public function getFields($scope): array
    {
        $fields = [];

        $fields['pec'] = $this->getFieldPec($scope);
        $fields['rec_code'] = $this->getFieldRecCode($scope);

        return $fields;
    }

    /**
     * Returns PEC field with validation-email= true
     *
     * @param string $scope
     * @return array
     */
    public function getFieldPec($scope): array
    {
        $attribute = $this->attributeRepository->get('customer_address', 'pec');

        return [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                'customScope' => $scope,
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input',
            ],
            'dataScope' => $scope . '.pec',
            'sortOrder' => $attribute->getSortOrder(),
            'visible' => true,
            'provider' => 'checkoutProvider',
            'validation' => [
                'validate-email' => true,
            ],
            'options' => [],
            'label' => __($attribute->getStoreLabel()),
        ];
    }

    /**
     * Returns "REC_CODE" field.
     *
     * @param string $scope
     * @return array
     */
    public function getFieldRecCode($scope): array
    {
        $attribute = $this->attributeRepository->get('customer_address', 'rec_code');

        return [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                'customScope' => $scope,
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input',
            ],
            'dataScope' => $scope . '.rec_code',
            'sortOrder' => $attribute->getSortOrder(),
            'visible' => true,
            'provider' => 'checkoutProvider',
            'validation' => $attribute->getValidationRules(),
            'options' => [],
            'label' => __($attribute->getStoreLabel()),
        ];
    }
}
