## Add 2 new fields to Address forms in Checkout, Adminhtml and Customer profile

This module adds 2 new fields ("PEC email" and "Recipient Code") to all Address customer forms. 'PEC email' will be validated to be an email address.    
Once order is created, the module will store these 2 new values to "quote_address" and "sales_order_address" table.
## 
## 

### 1.) Adding fields to Adminhtml
## 
We have to create EAV attributes and add them to attributeSet. Magento will manage DDBB storing by itself in Adminhtml section.
This way administrator could modify the customer fields without Customer invervention.

Check screenshot:

![Added Column](Docs/attributeInAdminhtml.gif)
##  
##  

### 2.) Add fields in Checkout

As Checkout steps are created dinamically we cannot use XML layouts, we must either:

* Create Plugin LayoutProcessor::afterProcess()
* or by D.I. using LayoutProcessorInterface

Source: https://devdocs.magento.com/guides/v2.4/howdoi/checkout/checkout_new_field.html

I chose D.I. option because many other users on the Internet use the plugin's way, and I wanted to choose the less common and, at the same time, the one which gives better performance.
##  
Fields are already shown in Checkout steps (shipping and billing address) but when customer creates new Address in Checkout step the new values weren't transferred correctly to the frontend.
Problem was that the Mixins from official documentation didn't work. Fix: https://github.com/experius/Magento-2-Module-Experius-ExtraCheckoutAddressFields/issues/11#issuecomment-519417639
##  
## 
### 3.) Manage DDBB operations

Several plugins will get the new values from checkout step and copy to 'quote' and 'order' objects when customer moves from shipping step to billing step and then to placing the order.

Two new columns were created in "sales_order_address" and "quote_address" DDBB tables (with declarative schema).
One plugin will handle the storage of these fields into those tables, so every order has filled in the new address fields information.
##  

Check screenshot:

![Added Column](Docs/checkout01.gif)
##  
##  
### 4.) Add fields to Customer profile
To add fields to Address form in Customer profile we have to override `edit.phtml` and add fields inside it. 
##  
Check screenshot:


![Added Column](Docs/attributeInCustomerProfile.gif)

##  
##  

