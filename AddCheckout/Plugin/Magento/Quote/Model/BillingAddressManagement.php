<?php
declare(strict_types = 1);

namespace Jose\AddCheckout\Plugin\Magento\Quote\Model;

use Magento\Quote\Api\Data\AddressInterface;

use Magento\Quote\Model\BillingAddressManagement as BillingAddressMgmt;

use Psr\Log\LoggerInterface;

/**
 * BEFORE PLUGIN: Saves new attributes to Address before Billing step is shown in Checkout
 */
class BillingAddressManagement
{
    protected $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param BillingAddressMgmt $subject
     * @param $cartId
     * @param AddressInterface $address
     * @param boolean $useForShipping
     * @return void
     */
    public function beforeAssign(
        BillingAddressMgmt $subject,
        $cartId,
        AddressInterface $address,
        $useForShipping = false
    ) {
        $extAttributes = $address->getExtensionAttributes();

        if (!empty($extAttributes)) {

            try {
                $address->setPec($extAttributes->getPec());
                $address->setRecCode($extAttributes->getRecCode());

            } catch (\Exception $e) {
                $this->logger->critical($e->getMessage());
            }
        }
    }
}
