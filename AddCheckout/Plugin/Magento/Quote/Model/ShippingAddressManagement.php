<?php
declare (strict_types = 1);

namespace Jose\AddCheckout\Plugin\Magento\Quote\Model;

use Magento\Quote\Api\Data\AddressInterface;
use Magento\Quote\Model\ShippingAddressManagement as ShippingMgmt;

use Psr\Log\LoggerInterface;

/**
 * BEFORE PLUGIN: Saves new attributes to Address before Shipping step is shown in Checkout
 */
class ShippingAddressManagement
{

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @param LoggerInterface
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param ShippingMgmt $subject
     * @param $cartId
     * @param AddressInterface $address
     * @return void
     */
    public function beforeAssign(ShippingMgmt $subject, $cartId, AddressInterface $address)
    {
        $extAttributes = $address->getExtensionAttributes();

        if (!empty($extAttributes)) {

            try {

                $address->setPec($extAttributes->getPec());
                $address->setRecCode($extAttributes->getRecCode());

            } catch (\Exception $e) {
                $this->logger->critical($e->getMessage());
            }
        }
    }
}
