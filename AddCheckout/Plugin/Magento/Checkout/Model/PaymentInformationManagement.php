<?php
declare(strict_types = 1);

namespace Jose\AddCheckout\Plugin\Magento\Checkout\Model;

use Magento\Quote\Api\Data\AddressInterface;
use Magento\Quote\Api\Data\PaymentInterface;

use Magento\Checkout\Model\PaymentInformationManagement as PaymentManagement;

use Psr\Log\LoggerInterface;

/**
 * BEFORE PLUGIN: Save new attributes to Address model before SavePaymentInformation
 */
class PaymentInformationManagement
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
    
    /**
     * @param PaymentManagement $subject
     * @param $cartId
     * @param PaymentInterface $paymentMethod
     * @param AddressInterface $address
     * @return void
     */
    public function beforeSavePaymentInformation(
        PaymentManagement $subject,
        $cartId,
        PaymentInterface $paymentMethod,
        AddressInterface $address
    ) {
        $extAttributes = $address->getExtensionAttributes();

        if (!empty($extAttributes)) {
            try {

                $address->setPec($extAttributes->getPec());
                $address->setRecCode($extAttributes->getRecCode());

            } catch (\Exception $e) {
                $this->logger->critical($e->getMessage());
            }
        }
    }
}
