<?php
declare (strict_types = 1);

namespace Jose\AddCheckout\Observer\Sales;

use Magento\Quote\Model\Quote;

use Magento\Sales\Model\Order;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;

/**
 * Observer: Copy the new attributes from Quote Address to Order Address when Quote is submitted
 */
class ModelServiceQuoteSubmitBefore implements ObserverInterface
{

    public function execute(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getEvent()->getOrder();
        /** @var Quote $quote */
        $quote = $observer->getEvent()->getQuote();

        if ($quote->getBillingAddress()) {
            $order->getBillingAddress()->setPec($quote->getBillingAddress()->getPec());
            $order->getBillingAddress()->setRecCode($quote->getBillingAddress()->getRecCode());
        }

        if ($order->getShippingAddress()) {
            $order->getShippingAddress()->setPec($quote->getShippingAddress()->getPec());
            $order->getShippingAddress()->setRecCode($quote->getShippingAddress()->getRecCode());
        }
    }
}
