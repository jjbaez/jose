define(
    [
         'Magento_Checkout/js/view/payment/default'
    ],
    function (Component) {
         'use strict';
         return Component.extend({
             defaults: {
                 template: 'Jose_Payment/payment/vivinopay-form'
             },
             
            /**
             * Get value of instruction field.
             * @returns {String}
             */
             getInstructions: function () {
                 return window.checkoutConfig.payment.custom_pay.instructions;
                }
         });
    }
    );