define(
    [
         'uiComponent',
         'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
         Component,
         rendererList
    ) {
         'use strict';
         rendererList.push(
             {
                 type: 'custom_pay',
                 component: 'Jose_Payment/js/view/payment/method-renderer/vivinopay-method'
             }
         );
         /** Add view logic here if needed */
         return Component.extend({});
    }
    );