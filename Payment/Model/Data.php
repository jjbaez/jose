<?php
declare(strict_types=1);

namespace Jose\Payment\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Data
{
    private const PAYMENT_INSTRUCTIONS = 'payment/custom_pay/instructions';

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return string|null
     */
    public function getPaymentInstructions(): ?string
    {
        return $this->scopeConfig->getValue(self::PAYMENT_INSTRUCTIONS, ScopeInterface::SCOPE_STORE);
    }
}
