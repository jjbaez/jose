<?php
declare(strict_types=1);

namespace Jose\Payment\Model\Ui;

use Jose\Payment\Model\Data;

use Magento\Checkout\Model\ConfigProviderInterface;

class ConfigProvider implements ConfigProviderInterface
{
    const CODE = 'custom_pay';

    /**
     * @var Data
     */
    private $scopeConfig;

    /**
     * @param Data $config
     */
    public function __construct(
        Data $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return array|null
     */
    public function getConfig(): ?array
    {
        $config = [
            'payment' => [
                self::CODE => [
                    'instructions' => $this->scopeConfig->getPaymentInstructions()
                ]
            ]
        ];
        return $config;
    }
}
